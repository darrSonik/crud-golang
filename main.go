package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/labstack/echo"
)

type Cat struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

type Dog struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

type Hamster struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

func yello(c echo.Context) error {
	return c.String(http.StatusOK, "yello from the web side!")
}

func getCats(c echo.Context) error {
	catName := c.QueryParam("name")
	catType := c.QueryParam("type")

	dataType := c.Param("data")

	switch dataType {
	case "string":
		return c.String(http.StatusOK, fmt.Sprintf("Your cat's name is \"%s\"\nand its type is \"%s\"", catName, catType))
	case "json":
		return c.JSON(http.StatusOK, map[string]string{
			"name": catName,
			"type": catType,
		})
	default:
		return c.JSON(http.StatusBadRequest, map[string]string{
			"error": "you need to specify data type",
		})
	}
}

// FASTEST
func addCat(c echo.Context) error {
	cat := Cat{}

	defer c.Request().Body.Close()

	b, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Printf("Failed reading the request: %s", err)
		return c.String(http.StatusInternalServerError, "")
	}

	err = json.Unmarshal(b, &cat)
	if err != nil {
		log.Printf("Failed unmarshaling in addCat: %s", err)
		return c.String(http.StatusInternalServerError, "")
	}

	log.Printf("This is your cat: %#v", cat)
	return c.String(http.StatusOK, "We got your cat!")
}

// Almost as fast as `addCat` - most preferred method
func addDog(c echo.Context) error {
	dog := Dog{}

	defer c.Request().Body.Close()

	err := json.NewDecoder(c.Request().Body).Decode(&dog)
	if err != nil {
		log.Printf("Failed reading the request: %s", err)
		// return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	log.Printf("This is your dog: %#v", dog)
	return c.String(http.StatusOK, "We got your dog!")
}

// Slowest - but speed difference is negligible unless used in a very big project
func addHamster(c echo.Context) error {
	hamster := Hamster{}

	defer c.Request().Body.Close()

	err := c.Bind(&hamster)
	if err != nil {
		log.Printf("Failed reading the request: %s", err)
		// return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	log.Printf("This is your hamster: %#v", hamster)
	return c.String(http.StatusOK, "We got your hamster!")
}

func main() {
	e := echo.New()

	e.GET("/", yello)
	e.GET("/cats/:data", getCats)

	e.POST("/cats", addCat)
	e.POST("/dogs", addDog)
	e.POST("/hamster", addHamster)

	e.Start(":8000")
}
