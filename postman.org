#+title: API Testing

Testing the API Requests for the server.

* GET request
#+begin_src restclient
GET http://localhost:8000/cats/json?name=akram&type=lifeless
#+end_src

#+RESULTS:
#+BEGIN_SRC js
{
  "name": "akram",
  "type": "lifeless"
}

// GET http://localhost:8000/cats/json?name=akram&type=lifeless
// HTTP/1.1 200 OK
// Content-Type: application/json; charset=UTF-8
// Date: Wed, 17 Aug 2022 11:41:38 GMT
// Content-Length: 35
// Request duration: 0.002376s
#+END_SRC

* POST request
#+begin_src restclient
POST http://localhost:8000/cats
Content-Type: application/json

{
	"name": "Hello",
	"type": "World"
}
#+end_src

#+RESULTS:
#+BEGIN_SRC text
We got your cat!
POST http://localhost:8000/cats
HTTP/1.1 200 OK
Content-Type: text/plain; charset=UTF-8
Date: Wed, 17 Aug 2022 12:17:03 GMT
Content-Length: 16
Request duration: 0.002122s
#+END_SRC


#+begin_src restclient
POST http://localhost:8000/dogs
Content-Type: application/json

{
	"name": "Hello",
	"type": "Dog"
}
#+end_src

#+RESULTS:
#+BEGIN_SRC text
We got your dog!
POST http://localhost:8000/dogs
HTTP/1.1 200 OK
Content-Type: text/plain; charset=UTF-8
Date: Wed, 17 Aug 2022 12:25:01 GMT
Content-Length: 16
Request duration: 0.002189s
#+END_SRC

#+begin_src restclient
POST http://localhost:8000/dogs
Content-Type: application/json

{
	"name": "Hello",
	"type": "Hamster"
}
#+end_src

#+RESULTS:
#+BEGIN_SRC text
We got your dog!
POST http://localhost:8000/dogs
HTTP/1.1 200 OK
Content-Type: text/plain; charset=UTF-8
Date: Wed, 17 Aug 2022 12:29:10 GMT
Content-Length: 16
Request duration: 0.002255s
#+END_SRC
